<?php
session_start();
include('functions_EM_2.php');
$timeout=sessionTimeout();
//Retrieve the parameters from the LogIn page and decide if it is correct or not.

    $email="";
    $password="";
    $Rol="";

  if(isset($_POST["email"]) && isset($_POST["password"]))
    {
        $email = $_POST["email"];
        $password = $_POST["password"];
        $_SESSION["Rol"]=getRol($email, $password);
        $timeout="";
    }

    //if(isset($_POST["rol"])) $_SESSION["Rol"]=$_POST['rol'];


    if(isset($_SESSION["Rol"])) $Rol=$_SESSION["Rol"];

    if(!empty($timeout)) {
        redirectToLogInError("TIMEOUT");
    }

    if(empty($Rol)){
        redirectToLogInError("CREDENTIALS");
    }

    function TryLogIn($email, $password) {
        $r = new HttpRequest('http://35.156.33.57:8080/bonita/', HttpRequest::METH_POST);
        $r->addPostFields(array('email' => '$email', 'password' => '$password', 'redirect' => 'false', 'redirectUrl' => ''));
        try
        {
            echo $r->send()->getBody();
        }
        catch (HttpException $ex) 
        {
            echo $ex;
        }
    }

    function getRol($email, $password) {
        $hashPass=hash("sha256", $password);

            include ("Mysqlconn.php");

            $query="SELECT RolARista FROM ARista.dbo.UsuariosBonita WHERE CorreoElectronicoEmpresa='$email'";
            // MYSQL
            // $RolARista_mysqli = $conexion->query($query);

            // SQL
            
            $RolARista_sql = sqlsrv_query( $conexion, $query);
            if($RolARista_sql == false){
                echo "its fake";
                echo print_r( sqlsrv_errors(), true);
            }else{
                echo "its true";
            }

            $RolARista="";

            while ($row = sqlsrv_fetch_array($RolARista_sql, SQLSRV_FETCH_NUMERIC)) 
            {
            if(empty($row[0])) return "";
            $RolARista=$row[0];      
            }

            $query="SELECT Password FROM ARista.dbo.RolesArista WHERE RolArista='$RolARista'";
            // MYSQL
            // $password_mysqli = $conexion->query($query);
            $password_sql = sqlsrv_query( $conexion, $query);

            while ($row = sqlsrv_fetch_array($password_sql, SQLSRV_FETCH_NUMERIC)) 
            {
            $hashPassARista=$row[0];      
            }

            if(strcmp($hashPass, $hashPassARista)=='0') return $RolARista;
            else redirectToLogIn();    
    }



function disableButtons($Rol) {
    if(strpos(strtolower($Rol),'admin') !== false) return;
    echo "var buttons=$('.dt-button btn pulse');
            for(var i=0;i<buttons.length;i++) {
                buttons[i].disable();
            }";
}

?>
<html>
    <head>
        <title>BilliB ARista</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- GOOGLE FONTS + MATERIAL ICONS -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- FAVICON -->
        <link rel="shortcut icon" type="image/png" href="https://billibfinance.com/wp-content/uploads/2017/10/favicom.png"/>
        <!-- FRAMEWORKS -->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>         
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">
        <link href="css/jquery.dataTables.min.css" rel="stylesheet">
        <!-- CSS -->
        <link rel="stylesheet" href="style/FirstPage.css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

        <!-- FRAMEWORK SCRIPTS -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/plug-ins/1.10.16/api/fnFilterOnReturn.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
        <script src="https://cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
        <script src="https://cdn.datatables.net/plug-ins/1.10.16/api/sum().js"></script>
        <script type="text/javascript" src="js/firstpage.js"></script>    
    </head>
    <body>
        <header id="BillibHeader">
        <!-- <div  class="row">
        <div class="col s12 m11">
        <h2 id="BHeader" align=\"center\">ARIsta_web. Aplicación de consulta y mantenimiento</h2>
        </div>
        <div style="padding-top: 20px;" class="col s12 m1">
            <a href="functions_route.php?logout=true" class="waves-effect waves-light btn">Log Out </a>
        </div>
        </div> -->
        <h2 id="BHeader" align=\"center\">ARIsta_web. Aplicación de consulta y mantenimiento</h2>
        </header>
        <!-- PRELOADER -->
        <!-- <div class="preloader-background">
            <h3 class="preloader-msg">Espere por favor, la aplicación está cargando</h3>
            <div class="preloader-wrapper big active">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div> -->
        <div id="Preloader">
    <h3 class="preloader-msg">Espere por favor, la aplicación está cargando</h3>
    <div class="preloader-wrapper big active">
      <div class="spinner-layer spinner-blue">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-red">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-yellow">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-green">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>
    </div>
        </div>
        <!-- CONTAINER -->
        <div class="container-fluid main-container">
            <div class="row">
                <!-- LEFT MENU -->
                <div class="col-sm-1 col-md-1 col-lg-1 menu">
                    <div id="leftMenu" class="menu-views">
                                            
                    </div>
                </div>
                <!-- TABLES -->
                <div id="table_container" class="col-sm-11 col-md-11 col-lg-11 tables-container">
                    
                </div>
                <!--<div id="border"></div>-->                  
            </div>
        </div>  
        <footer id="BillibFooter">
            <p style="position: relative; left: 39vw; bottom: 0px; font-weight: 600; font-size: 13px; color: #3a3a3a;">Powered by dyTAB</p>
            <div class="container">
                <center><a href="http://proceedit.blogspot.com.es/" style="color:black; font-size: 13px; font-weight: 600;">Copyright © 2018 Proceedit, all rights reserved.</a>
            </div>
        </footer>       
    </body>
    <!-- OUR SCRIPT! -->
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>-->
    <!--<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>-->
    <script type="text/javascript" language="javascript" class="init">
    let userRol = "<?php echo $Rol; ?>";
    function openTab(tab_name){ 
        var alltabs = document.getElementsByClassName("tab-menu");
        var tab = document.getElementById(tab_name);

   for(i = 0; i <  alltabs.length; i++) {
            alltabs[i].style.backgroundColor = "";
            alltabs[i].style.color = "";
        }

        tab.style.color = "#00ffd1";
        tab.style.backgroundColor = "#15202b";
        tab.style.borderRadius = "3px";
        tab.style.width = "auto";

    }

    function setInnerText(obj, text) {
                var object = document.getElementById(obj);
                object.innerText = text;
            }
    
    function displaytable(table, allviews) 
            {
                var views = allviews.split(",");

                for(var i=0;i<views.length;i++) 
                {
                    var x = document.getElementById("div_" + views[i].trim());
                    x.style.display = "none";
                }
                var y = document.getElementById("div_" + table.trim());
                y.style.display = 'block';
                $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
            }

    let url_reqs = "functions_route.php";

    function buildLeftBar(){
        $.ajax({
            type: "GET",
            url: url_reqs,
            data: {"left_menu" : true, "Rol": userRol},
            dataType: "json",
            success: function (response) {
                let menu = "<ul>";
                response.tabs.forEach(element => {
                    menu += "<li><a class=\"tab-menu\" id=\"tab-"+element+"\" onclick=\"openTab('tab-"+element+"'),setInnerText('BHeader', '"+element+" - Vista datos')\"href=\"javascript:displaytable('"+element+"View', '"+response.allViews+"');\">"+element+"</a></li>"
                });
                menu +="</ul>";
                $("#leftMenu").html(menu);
            }
        });
    }
    function selectInitiate(){
            $("select").val('10');
            $('select').addClass("browser-default");
            $('select').material_select();
            }

    function searchersInitiate(){

            $('.ColumnSearcher').each( function () {
            let title = "Buscar";
            $(this).html( '<input type="text" placeholder="'+title+'" />' );
            });

            $('.minimum').each( function () {
            let title = "Min";
            $(this).html( '<input type="text" placeholder="'+title+'" value =""/>' );
            });

            $('.maximum').each( function () {
            let title = "Max";
            $(this).html( '<input type="text" placeholder="'+title+'" value =""/>' );
            });
        }
    let filtersMap = {};
    let exceptionsView = ["FacturasView", "GruposView", "RolesView", "UsuariosView", "MembresiasView"];
    let exceptionsFormat = ["int","long", "smallint", "bigint", "float", "numeric", "double", "decimal"];
    function buildTableContainer(dataTableFunc){
        $.ajax({
            type: "POST",
            url: url_reqs,
            data: {"create_tables" : true, "Rol": userRol},
            dataType: "json",
            success: function (response) {
                
                let tables = "";
                
                response.tables.forEach(model => {
                    let cells=[];
                    let table = "";
                    let col = 0;
                    table += "<div id=\"div_"+model.name+"\" style=\"display:none; position:absolute; top: 4%; left: 4%; width:100%; table-layout:fixed\">";
                    table += "<table id=\""+model.name+"\" class=\"display dataTable\" cellspacing=\"0\" width=\"100%\">";
                    table += "<thead>";
                    table += "<tr role=\"row\">";
                    let footer ="<tfoot>";
                    let frow1 = "<tr>";
                    let frow2 ="<tr id=\"partials_"+model.name+ "\">";
                    let frow3="<tr id=\"totals_" +model.name+ "\">";
                    let frow4="<tr id=\"minimums_" +model.name+ "\">";
                    let frow5="<tr id=\"maximums_" +model.name+ "\">";
                    if(!exceptionsView.includes(model.name) && userRol =="admin")
                    {
                        table += "<th class=\"actions\" tabindex=\"0\" rowspan=\"1\" colspan=\"1\" align=\"left\" aria-label=\"Actions\" display=\"inline-block;\" type=\"hidden\">Actions</th>";
                        frow1+="<td class='actionsColumn'></td>";
                        frow2+="<td class='actionsColumn'></td>";
                        frow3+="<td class='actionsColumn'></td>";
                        frow4+="<td class='actionsColumn'></td>";
                        frow5+="<td class='actionsColumn'></td>";
                    }                   

                    model.columns.forEach(column => {
                        cells[col++]={"filter": "", "range":"", "active":false, "max": "", "min": "", "type": column.type};
                        let sum ="";
                        if(exceptionsFormat.some(function(element){
                            return column.type.toLowerCase().includes(element);
                        })){
                            sum = "class=\"sum\"";
                            frow2+="<td class=\"partials\" style=\"display:none;\"></td>";
                            frow3+="<td class=\"totals\" style=\"display:none;\"></td>";	
                        }else{
                            frow2+="<td class=\"emptyPartials\"></td>";
                            frow3+="<td class=\"emptyTotals\"></td>";
                        }
                        if(exceptionsFormat.some(function(element){
                            return column.type.toLowerCase().includes(element);
                        }) || column.type.toLowerCase() == 'date'){
                            frow4+="<td class=\"minimum\" data-type="+column.type+" data-value=\"\" style=\"display:none;\"></td>";
                            frow5+="<td class=\"maximum\" data-type="+column.type+" data-value=\"\" style=\"display:none;\"></td>";
                        }else{
                            frow4+="<td class=\"emptyMinimum\" data-type='empty'></td>";
                            frow5+="<td class=\"emptyMaximum\" data-type='empty'></td>";
                        }
                        table+= "<th max-height: 50px; tabindex=\"0\" " +sum;
                        table+= " style=\"text-align:left;"+column.width+"\" rowspan=\"1\" colspan=\"1\" aria-label=\"";
                        table+= column.name+": activate to sort column descending\" display=\"inline-block\" aria-sort=\"ascending\" type=\"hidden\">"+column.name+"</th>";
                        frow1+="<td class=\"columnSearcher Filter"+model.name+"\" style=\"display:none;\">Buscar</td>";

                    });
                    table+="</tr> </thead>";
                    table+="<tbody></tbody>";
                    frow1+="</tr>";
                    frow2+="</tr>";
                    frow3+="</tr>";
                    frow4+="</tr>";
                    frow5+="</tr>";
                    filtersMap[model.name]=cells;
                    footer+=frow1+frow2+frow3+frow4+frow5+"</tfoot>";
                    table+=footer;
                    table+="</table>";
                    table+="</div>";

                    tables += table;

                });
                $("#Preloader").hide()
                $("#table_container").html(tables);

                dataTableFunc();
                
            }
        });
    }
    function columnSearcherCleaner(View) {
            $('.Filter' + View).each( function () {
                if($(this).find('input').val().length) 
                {
                    $(this).find('input').attr("placeholder", "Buscar");
                    $(this).find('input').val('');
                    $(this).find('input').change();
                }
                                                                          
            });
        }
        function minCleaner() {
            $('.minimum').each( function () {
                if($(this).find('input').val()) {
                    $(this).find('input').attr("placeholder", "Min");
                    $(this).find('input').val('');
                    $(this).find('input').change();
                }
            });
        }

        function maxCleaner() {
            $('.maximum').each( function () {
                if($(this).find('input').val()) {
                    $(this).find('input').attr("placeholder", "Max");
                    $(this).find('input').val('');
                    $(this).find('input').change();
                }
            });
        }
    
    function buildTableContent(){
        let dts = Array.from(document.getElementsByClassName('dataTable'));
            dts.forEach(dtable => {
                let currentTable = $("#"+dtable.id).DataTable({
                // "bProcessing":true,
                // "Processing": true,
                "serverSide":true,
                "ajax":{
                    url: url_reqs,
                    type: "POST",
                    data:{"fetch_data":true, "view": dtable.id, "Rol": userRol},
                    error: function(){
                        $("#dataTable_processing").css("display","non");
                    },
                
                },
                "columnDefs":[{ 
                "targets":'actions',
                "orderable":false
                }],
                scrollX:true,
                select: true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json",
                        select:{
                            rows:", %d filas seleccionadas"
                        }
                    },
                    dom: 'Blfrtip',
                    buttons : [
                        {
                            text: 'Filtros',
                            action: function(e, dt, node, config){
                                let rows = document.getElementsByClassName('columnSearcher');
                                        let hide;
                                        for(let i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            hide=0;
                                            }
                                            else {
                                                rows[i].style.display='none';
                                                hide=1;   
                                            }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                columnSearcherCleaner(dtable.id);
                                            }
                                        else 
                                        {
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                            },
                            "className":'btn btn-filtros'
                        },
                        {
                            text: 'Rangos',
                            action: function (e, dt, node, config) {
                                        let mins = document.getElementsByClassName('minimum');
                                        let maxs = document.getElementsByClassName('maximum');
                                        for(let i=0;i<mins.length;i++) {
                                            if(mins[i].style.display =='none') {
                                            mins[i].setAttribute('style', '');
                                            maxs[i].setAttribute('style', '');
                                            hide = 0;
                                            
                                        }
                                        else {
                                            mins[i].style.display='none';
                                            maxs[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                minCleaner();
                                                maxCleaner();
                                        }
                                        else 
                                        {
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        let rows = document.getElementsByClassName('partials');
                                        let raws = document.getElementsByClassName('emptyPartials');
                                        let hide;
                                        for(let i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide=0;
                                            
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide=1;
                                        }
                                        }
                                       /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        let rows = document.getElementsByClassName('totals');
                                        let raws = document.getElementsByClassName('emptyTotals');
                                        let hide;
                                        for(let i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                                rows[i].setAttribute('style', '');
                                                raws[i].setAttribute('style', '');
                                                hide=0;
                                                
                                            }
                                            else {
                                                rows[i].style.display='none';
                                                raws[i].style.display='none';
                                                hide=1;
                                            }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'
                                },
                                {
                                    extend: 'selectAll',
                                        action : function(e, dt, button, config){
                                            selectbool = true;
                                            getAll(dtable.id, selectAll);
                                            
                                        },
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    bBomInc: true,
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible:not(:eq(0))',
                                    rows:'.selected',
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible:not(:eq(0))',
                                },
                                "className":'btn'
                                },
                                {
                                  text: 'Nuevo Registro<i class=\"material-icons right\" >add_box</i>',
                                  action: function ( e, dt, button, config ) {
                                    window.location = 'Form.php?ViewName='+dtable.id;
                                  },
                                  "className":'btn pulse'        
                                }
                    ],
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],                    
                    initComplete: function () {
                        if(exceptionsView.includes(dtable.id) || userRol !="admin"){
                            $('#'+dtable.id).DataTable().button(".pulse").remove();
                        }
                    },
                    footerCallback: function (row, data, start, end, display) {
                        let api=this.api();
                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        let num = index;
                        let sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                            let cell = $('tr:eq(1) td:eq(' + num + ')', currentTable.table().footer());
                            cell.html(parseFloat(sum).toFixed(2));
                        });

                        let totalsJson =api.ajax.json().totals[0];
                        totalsJson.forEach((element, index) => {
                        let num = index;

                        let sum = element;
                        if(sum === null){
                            sum =0;
                        }

                        if(sum >= 0){
                            let cell = $('tr:eq(2) td:not(".actionsColumn"):eq(' + num + ')', currentTable.table().footer());
                            if($(cell).hasClass("totals")){
                            cell.html(parseFloat(sum).toFixed(2));
                            }
                        }
                        });
                    },
                    preDrawCallback: function() {
                        $("#Preloader").show();
                    },
                    drawCallback: function(){
                        $("#Preloader").hide();
                    }

                });

                $('#' + dtable.id + ' tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
                } );

            });

            selectInitiate();
            searchersInitiate();
            activateFilterSearch();   
    }
    function getAll(dtableid, callback){
        if($("#"+dtableid).DataTable().page.len()!=-1){
            $("#"+dtableid).DataTable().page.len(-1).draw();
            $("#"+dtableid).on("draw.dt", function(){
                callback(dtableid);
            })
        }else{
            callback(dtableid);
        }

    }
    let selectbool = false;
    function selectAll(dtableid){
        if(selectbool){
        $("#"+dtableid).DataTable().rows({ search: 'applied'}).select();
        }
        selectbool=false;
    }
    function activateFilterSearch(){
        $(".dataTable").each(function (index, table) {
                $(table).find(".columnSearcher").each(function(i, cs){
                        $(cs).children('input').on('change', function(){
                            filterCell = filtersMap[$(table).attr("id")][i];
                            //assign the input value on the column
                            filterCell.filter = this.value;
                            //activate the cell if at least one field is not empty
                            filterCell.active= (filterCell.filter != "" || filterCell.range !="");
                            //do search if cell is active
                            if(filterCell.active){
                                $(table).DataTable().columns(i)
                                        .search(filterCell.filter+"|"+filterCell.range);  
                            }else{
                                $(table).DataTable().columns(i)
                                        .search("");
                            }
                            $(table).DataTable().draw();
                        });
                    });
            $(table).find("#minimums_"+$(table).attr("id"))
            .find("td:not('.actionsColumn')")
            .each(function(i, td){
                $(td).children("input").on('change', function(){
                    let filterCell = filtersMap[$(table).attr("id")][i];
                    let max = filterCell.max;
                    filterCell.min=$(this).val();
                    let min = filterCell.min;
                    let search ="";
                    let empty = (max == "" && min=="");
                    
                    if(min!=""){
                        if(filterCell.type=="date"){
                            if(max==""){
                                max="3000-12-12";
                            }
                        }else{
                            if(max==""){
                                max=Number.MAX_SAFE_INTEGER;
                            }
                        }
                        search+="Range;"+min+";"+max+"";
                    }else{
                        if(max!=""){
                            if(filterCell.type =="date"){
                                min="1800-01-01";
                            }else{
                                min=-1*Number.MAX_SAFE_INTEGER;
                            }
                            search+="Range;"+min+";"+max+"";
                        }
                    }
                    if(search != ""){
                        if(filterCell.type =="date"){
                            search+=";DATE";
                        }else{
                            search+=";NUM";
                        }
                    }
                    filterCell.range=search;
                    //activate the cell if at least one field is not empty
                    filterCell.active= (filterCell.filter != "" || filterCell.range !="");
                    //do search if cell is active
                    if(filterCell.active){
                        $(table).DataTable().columns(i)
                                .search(filterCell.filter+"|"+filterCell.range);  
                    }else{
                        $(table).DataTable().columns(i)
                            .search("");
                    }
                        $(table).DataTable().draw();
                });
            });
        $(table).find("#maximums_"+$(table).attr("id"))
            .find("td:not('.actionsColumn')")
            .each(function(i, td){
                $(td).children("input").on('change', function(){

                    let filterCell = filtersMap[$(table).attr("id")][i];
                    let min = filterCell.min;
                    filterCell.max=$(this).val();
                    let max = filterCell.max;
                    let search ="";
                    let empty = (min == "" && max=="");
                    
                    if(max!=""){
                        if(filterCell.type=="date"){
                            if(min==""){
                                min="1800-01-0";
                            }
                        }else{
                            if(min==""){
                                min=-1*Number.MAX_SAFE_INTEGER;
                            }
                        }
                        search+="Range;"+min+";"+max+"";
                    }else{
                        if(min!=""){
                            if(filterCell.type =="date"){
                                max="3000-12-12";
                            }else{
                                max=Number.MAX_SAFE_INTEGER;
                            }
                            search+="Range;"+min+";"+max+"";
                        }
                    }
                    if(search != ""){
                        if(filterCell.type =="date"){
                            search+=";DATE";
                        }else{
                            search+=";NUM";
                        }
                    }
                    filterCell.range=search;
                    //activate the cell if at least one field is not empty
                    filterCell.active= (filterCell.filter != "" || filterCell.range !="");
                    //do search if cell is active
                    if(filterCell.active){
                        $(table).DataTable().columns(i)
                                .search(filterCell.filter+"|"+filterCell.range);  
                    }else{
                        $(table).DataTable().columns(i)
                            .search("");
                    }
                        $(table).DataTable().draw();
                    
                });
            });
        });
    }
    
    $(document).ready(function(){
        buildLeftBar();
        buildTableContainer(buildTableContent);
        
        $('.datepicker').pickadate(
                { 
                selectMonths: true,
                 selectYears: 15, 
                 today: 'Today',
                 format: 'yyyy-mm-dd', 
                 clear: 'Clear',
                 close: 'Ok',
                 closeOnSelect: false,
                 formatSubmit: 'yyyy-mm-dd'
                });
    
    });

    
    </script>

</html>
