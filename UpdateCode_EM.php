<?php

include('functions_EM_2.php');
include('Mysqlconn.php');

session_start();

if(isset($_SESSION["Rol"]))
    {
       $Rol=$_SESSION["Rol"];
    }
else 
  {
    redirectToLogIn();
  }

  if(isset($_POST["TableName"]))
    {
        $TableName = $_POST["TableName"];
    }

  if(isset($_POST["Id"]))
    {
        $id_array = $_POST["Id"];
    }

$columnNames=getColumnNames_table($TableName);

$ViewName=getViewFromTable($TableName);

$values=array();
$passedcolumnNames=array();

for($i=0;$i<sizeof($columnNames);$i++) 
{
  $RealFieldName=check_if_PK($columnNames[$i], $TableName);

  if(isset($_POST["$RealFieldName"]))
      {
        array_push($values, $_POST["$RealFieldName"]);
        array_push($passedcolumnNames, $columnNames[$i]);
      }
}


function getValueFromPK($TableName, $values, $key) {

  include('Mysqlconn'); //Sebastian

  $columnNames=getColumnNames_table($TableName);
  $k=array_search($key, $columnNames);

  if($key=="IdClienteArista" && $k==false) {
   $k=array_search("CIFNIFNIE", $columnNames);
    $query= "SELECT IdClienteArista from ARista.dbo.Clientes where CIFNIFNIE='" . $values[$k] . "'";

    $col_sql=sqlsrv_query($conexion, $query);
    $col="";
    while($row = sqlsrv_fetch_array($col_sql, SQLSRV_FETCH_NUMERIC))
    {
      $col=$row[0];
    }

    $query= "SELECT IdClienteArista from ARista.dbo.$TableName where IdClienteArista='" . $col . "'";
    $col_sql=sqlsrv_query($conexion, $query);
    $col="";
    while($row = sqlsrv_fetch_array($col_sql, SQLSRV_FETCH_NUMERIC))
    {
      $col=$row[0];
    }
    return $col;
  }

  if($k!==false){
    return $values[$k];
  }
  else {
    $query="Select ColumnName from ARista.dbo.ForeignKeysAux where ComboValue='$key'";
    $col_sql=sqlsrv_query($conexion, $query);
    while($row = sqlsrv_fetch_array($col_sql, SQLSRV_FETCH_NUMERIC))
    {
      $col=$row[0];
    }
    $k=array_search($col, $columnNames);
    return $values[$k];
  }
}

function createUpdateResult($TableName, $values, $columnNames, $id_value){

  $key=getPKfromTable($TableName);

/*  switch ($TableName) {
    case "Campanas":
    $RealFieldName=check_if_PK("NombreCampana", $TableName);
    $value=$_POST[$RealFieldName];
    $key="NombreCampana";
    break;

    case "AccionesSeguimientoOportunidades":
    $key="IdAccionSeguimientoOportunidad";
    $value="-1";
    break;

    case "Facturas":
    $key="IdFacturaInterno";
    $value=$_POST["IdFacturaInterno"];
    break;

    case "Oportunidades":
    $key="IdOperacion";
    $value=$_POST["IdOperacion"];
    break;

    default:
    $cif=$_POST["CIFNIFNIE"];
    $value=CIF_to_ID($cif);
    break;
  }*/

  updateData($TableName, $columnNames, $values, $id_value);

}

function CIF_to_ID($cif) {
  include('Mysqlconn'); //Sebastian
  $query="Select IdClienteArista from ARista.dbo.Clientes where CIFNIFNIE='$cif'";
    $col_sql=sqlsrv_query($conexion, $query);
    $col="";
    while($row = sqlsrv_fetch_array($col_sql, SQLSRV_FETCH_NUMERIC))
    {
      $col=$row[0];
    }
    return $col;
}

function value_to_idvalue($table, $id, $value, $valuecolumn) {
  include('Mysqlconn'); //Sebastian
  $query="Select $id from ARista.dbo.$table where $valuecolumn='$value'";
    $col_sql=sqlsrv_query($conexion, $query);
    while($row = sqlsrv_fetch_array($col_sql, SQLSRV_FETCH_NUMERIC))
    {
      $col=$row[0];
    }
    return $col;

}

function updateData($TableName, $columnNames, $values, $id_value) {
  include('Mysqlconn.php'); //Sebastian
  $ID = explode("_", $id_value);

  $update_values=getUpdateValues($values, $columnNames, $TableName);

  $updatequery=create_update_query($TableName, $columnNames, $values);

  $updatequery="Update ARista.dbo.$TableName set ";
  for($i=0;$i<count($columnNames);$i++) 
  {
    if(empty($update_values[$i]) && (string) $update_values[$i]!=='0' || (string) $update_values[$i]==='') {
      $update_sql_value="NULL";
      // $update_sql_value="''";
    }else{
      $update_sql_value="'".str_replace("'", "''", $update_values[$i])."'";
    }
    $updatequery .= $columnNames[$i] . "= $update_sql_value, ";
  }

  $updatequery=substr($updatequery, 0, -2);
  $updatequery.= " where " . $ID[0] . "='" . $ID[1] . "'";

  // try 
  // {
  //   //TEEEEEEST
  //     execute_mysql($updatequery, $columnNames, $update_values);
  //     echo "El registro se ha actualizado correctamente.";

  // }

  // catch(PDOException $e)
  // {
  //   echo "Error: " . $e->getMessage();
  //   return false;
  // }
  // echo $updatequery;
  if(sqlsrv_query($conexion, $updatequery) == false){
    echo "\n $updatequery";
    echo "Error no se pudo actualizar el registro correctamente. ";
    die( print_r( sqlsrv_errors(), true));
  }else{
    echo "\nEl registro se ha actualizado correctamente.";
  }

}


function check_if_exists($table, $key, $value) {
  include('Mysqlconn');
  $query="SELECT $key from ARista.dbo.$table where $key='$value'";
  $values_sql=sqlsrv_query($conexion, $query);
  $ret="";
  while($row = sqlsrv_fetch_array($values_sql, SQLSRV_FETCH_NUMERIC))
    {
      $ret=$row[0];
    }

return $ret;
}

function getPKfromTable($table) 
{
  switch ($table) {
    case 'Clientes':
    return "IdClienteArista";
    case 'ClientesDatosOperativosDet':
    return "IdClienteArista";
    case 'ClientesDatosEconomicosDet':
    return "IdClienteArista";
    case 'ClientesConfiguracionesOperativasDet':
    return "IdClienteArista";
    case 'Campanas':
    return "IdCampana";
    case 'Facturas':
    return 'IdFacturaInterno';
    case 'Oportunidades':
    return "IdOperacion";
    case 'PersonasContactoClientes':
    return "IdPersonaContactoClienteArista";
  }

}

function execute_mysql($query, $columnNames, $values) 
  {
    // $conn = new PDO("mysql:host=nettit-rds-read-replica.cqs6chtgxb5c.us-east-2.rds.amazonaws.com;dbname=ARista;charset=utf8", "root", "G4fFHDu#293u");
    // $conn = new PDO("mysql:host=check-acciones.cbclcaapul7u.eu-west-1.rds.amazonaws.com;dbname=ARista", "root", "G4fFHDu#293u");

    // $conn = new PDO("mysql:host=nettit-rds.cbclcaapul7u.eu-west-1.rds.amazonaws.com;dbname=ARista", "root", "G4fFHDu#293u");
  
    // set the PDO error mode to exception
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
      $conn->exec("set names utf8");

      $stmt = $conn->prepare($query);

      for($i=0;$i<sizeof($columnNames);$i++) 
      {
        if(empty($values[$i]) && (string) $values[$i]!=='0') {
          $values[$i]=NULL;
          // $values[$i]="NULL";
          // $stmt->bindParam($columnNames[$i], $values[$i],PDO::PARAM_NULL);
        }
        // else{
          $stmt->bindParam($columnNames[$i], $values[$i], PDO::PARAM_STR);
        // }
        
      
    }
      $stmt->execute();
  }

function check_if_PK($col_name, $TableName)
{
  include ("Mysqlconn.php"); //Sebastian
  $querycheck="SELECT ReferencedTableName, ComboValue from ARista.dbo.ForeignKeysAux where TableName='" . $TableName . "' and ColumnName='" . $col_name . "'";  
  $finalvalues_sql=sqlsrv_query($conexion, $querycheck);
  $finalvalues=array();

  while($row = sqlsrv_fetch_array($finalvalues_sql, SQLSRV_FETCH_NUMERIC))
    {
      array_push($finalvalues, $row[0]);
      array_push($finalvalues, $row[1]);
    }

  if(sizeof($finalvalues)==0) return $col_name;
  else return $finalvalues[1];

}

function checkPK_table_inverse($col_name, $TableName)
{
  include ("Mysqlconn.php"); //Sebastian
  $querycheck="SELECT ReferencedTableName, ReferencedColumnName from ARista.dbo.ForeignKeysAux where ColumnName='" . $col_name . "' and TableName='" . $TableName . "'";  

  $finalvalues_sql=sqlsrv_query($conexion, $querycheck);
  $finalvalues=array();

  while($row = sqlsrv_fetch_array($finalvalues_sql, SQLSRV_FETCH_NUMERIC))
    {
      array_push($finalvalues, $row[0]);
      array_push($finalvalues, $row[1]);
    }

  if(empty($finalvalues)) return "";

  //CIFNIFNIE CLIENTES
  return $finalvalues;
}

function getUpdateValues($values, $columnNames, $TableName) 
{
  include ("Mysqlconn.php");
  $table_field_value=array(array());
  $final_values=array();


  for($i=0;$i<sizeof($columnNames);$i++) 
  {
    $table_column=checkPK_table_inverse($columnNames[$i], $TableName);
    if(empty($table_column)) 
    {
      $to_push=array($TableName, $columnNames[$i], $values[$i]);
      array_push($table_field_value, $to_push);
      array_push($final_values, $values[$i]);
      continue;
    }

    else 
    {
      if(empty($values[$i])) 
      {
        array_push($final_values, NULL);
      }
      else 
      {
      $id_input=IdfromCombo($table_column[0], $table_column[1], $values[$i], $columnNames[$i], $TableName);
      $to_push=array($table_column[0], $table_column[1], $id_input);
      array_push($table_field_value, $to_push);
      array_push($final_values, $id_input);
      }
      
    }
  }

  return $final_values;

}

function IdFromCombo($table, $column, $value, $columnvalue, $maintable) 
{
  include ("Mysqlconn.php"); //Sebastian
  $RealFieldName=check_if_PK($columnvalue, $maintable);
  $querycheck="SELECT $column from ARista.dbo.$table where $RealFieldName='$value'";

  $finalvalues_sql=sqlsrv_query($conexion, $querycheck);

  while($row = sqlsrv_fetch_array($finalvalues_sql, SQLSRV_FETCH_NUMERIC))
    {
      $ID=$row[0];
    }

  //CIFNIFNIE CLIENTES
  return $ID;
}

function getTable($colName) 
{
  include ("Mysqlconn.php");
  $query="SELECT TableName from MaintainanceAux1 where Alias='$colName'";
  $table_sql=sqlsrv_query($conexion, $query);
  while ($row = sqlsrv_fetch_array($table_sql, SQLSRV_FETCH_NUMERIC)) 
    {
        $table=$row[0];   
    }
return $table;

}

function getTablesToInsert($insert_values)
{
  $tables=array();

  $rowNumber=count($insert_values);

  for($i=1;$i<$rowNumber;$i++) 
  {
    if(in_array($insert_values[$i][0], $tables)) 
    {
      continue;
    }
    else 
    {
      array_push($tables,$insert_values[$i][0]);
    }
  }
    return $tables;
}

function create_insert_query($TableName, $columnNames, $values) 
{
  $query="Update ARista.dbo.$TableName (";
  for($i=0;$i<count($columnNames);$i++) 
  {
    $query = $query . $columnNames[$i] . ", ";
  }
  return $query;
}

function create_update_query($TableName, $columnNames, $values) 
{
  $query="Update ARista.dbo.$TableName set ";
  for($i=0;$i<count($columnNames);$i++) 
  {
   
    $query = $query . $columnNames[$i] . "=:" . $columnNames[$i] . ", ";
  }
  return $query;
}

function createOptionButtons($ViewName, $Rol) {
  if(empty($Rol)) return;
  echo "<a href='./EFirstPage.php' class=\"btn waves-effect waves-light\"><i class=\"material-icons right\" >add_circle</i>Hacer otro registro</a><br>";
  echo "<form class=\"col s12\" method=\"POST\" action=\"./EFirstPage.php\">";
  echo "<input type =\"hidden\" name=\"rol\" value=\"$Rol\"/>";
  echo "<button class=\"btn waves-effect waves-light\" type=\"submit\"><i class=\"material-icons right\">home</i>Volver a inicio</button>";
  echo "</form>";
}



?>

<html lang="es">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="https://billibfinance.com/wp-content/uploads/2017/10/favicom.png"/>
    <!-- GOOGLE FONTS + ICONS -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>
<!-- CUSTOM CSS -->
    <link type="text/css" rel="stylesheet" href="style/Nuevoregistro_Form.css"/>
    <title>BilliB ARista</title>
    <link type="text/css" rel="stylesheet" href="style/EndPage.css"/>
    </head>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
    <script>
      function goBackTwice() {
          window.history.go(-2);
        }

        function goBack() {
          window.history.back();
        }
    </script>
    <header id="BillibHeader"><h2 align=\"center\">Actualización de datos en ARista</h2></header>
      <div class="row">
        <div class="col s12 offset-s4">
          <div class="card">
            <div class="card-update">
              <div class="card-content white-text">
                <?php
                createUpdateResult($TableName, $values, $passedcolumnNames, $id_array);
                ?>
                <br>
                <?php
                createOptionButtons($ViewName, $Rol);
                ?>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <br>
      <footer id="BillibFooter">
        <p style="position: relative; left: 39vw;  bottom: 13px; font-weight: 600; font-size: 13px; color: #3a3a3a;">Powered by dyTAB</p>
        <div class="container">
          <center><a href="http://proceedit.blogspot.com.es/" style="color:black; font-size: 13px; font-weight: 600;">Copyright © 2018 Proceedit, all rights reserved.</a>
        </div>
      </footer>
    <!-- <body>

      <?php
      createUpdateResult($TableName, $values, $passedcolumnNames, $id_array);
      ?>
      <br>
      <?php
      createOptionButtons($ViewName);
      ?>

    </body> -->
</html>