<?php

$clientes_nombre=0;

$clientes_conf=0;

function displayLogInErrorMessage() {
	echo "<div id=\"logInError\" class=\"container\">";
	echo "<div class=\"row\">
          <div class=\"input-field col s12 center\">
            <img src=\"images/BillibLogoTransparentLogIn.png\" alt=\"Billib Logo\" class=\"responsive-img\">
          </div>
        	</div>
  				<div class=\"row\">
    				<div class=\"input-field col s12 center\">
      					<span>Credenciales incorrectas. Por favor, inicie sesión.</span>
    				</div>
    			</div>
    			<div class=\"row\">
    				<a href=\"./LogIn.php\"><button class=\"btn waves-effect waves-light col s4 offset-s4\" name=\"logInPage\">Iniciar sesión<i class=\"material-icons left\">person</i></button></a>
		  		</div>
		  	</div>";
		  	return;

}

function redirectToLogInError($reason) {
	if($reason=="CREDENTIALS") echo "<meta http-equiv=\"refresh\" content=\"0; URL='./LogIn.php?wrong=1'\"/>";
	if($reason=="TIMEOUT") echo "<meta http-equiv=\"refresh\" content=\"0; URL='./LogIn.php?wrong=2'\"/>";
	
}

function getFieldType($table, $column) {
	include ("Mysqlconn.php");

    $query = "SELECT DATA_TYPE FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_CATALOG = 'ARista' AND TABLE_NAME = '$table' AND COLUMN_NAME = '$column'";
    $Views_sql = sqlsrv_query( $conexion, $query);
    $type="";

	if($Views_sql == false){
		echo( print_r( sqlsrv_errors(), true));
		die("$Views_sql");
	} 
	while ($row = sqlsrv_fetch_array($Views_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		$type=$row[0];		
	}

    return $type;
}

function getAllTypes($View) {

	include ("Mysqlconn.php");


    $query="SELECT DATA_TYPE FROM information_schema.COLUMNS where TABLE_SCHEMA = 'dbo' AND TABLE_CATALOG='ARista' AND TABLE_NAME = '$View' AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%'";
    //  DATA_TYPE FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_CATALOG = 'ARista' AND TABLE_NAME = '$View' AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%'
    $Views_sql = sqlsrv_query( $conexion, $query);
    $types=array();

	while ($row = sqlsrv_fetch_array($Views_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		array_push($types,$row[0]);	
	}


    return $types;
	
}

function retrieveViewList() 
{

	include ("Mysqlconn.php");

	// $query = "SHOW FULL TABLES IN ARista WHERE TABLE TYPE LIKE 'VIEW'";
	$query = "SELECT TABLE_NAME from [ARista].[INFORMATION_SCHEMA].[TABLES] where TABLE_TYPE Like 'VIEW'";
    $Views_sql = sqlsrv_query( $conexion, $query);
    $Views = array();

	while ($row = sqlsrv_fetch_array($Views_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
		array_push($Views, $row[0]);		
    }

    return $Views;
}

function createLeftBar($array, $Rol) 
{
	if(empty($Rol)) {
		return;
	}
	$rows=sizeof($array);
	$viewArray=$array;
	for($i=0;$i<sizeof($array);$i++) {
		$array[$i]=str_replace("View","", $array[$i]);
	}
	$string=getStringFromArray($viewArray);

    $response = array("tabs"=>$array, "allViews"=>$string);
    $jsonResponse = json_encode($response);
    
    return $jsonResponse;
}

function getStringFromArray($array) {
	$string="";
	for($i=0;$i<sizeof($array);$i++) {
		$string=$string . "$array[$i]" . ", ";
	}
	$string=substr($string, 0, -2);
	return $string;
}


function splitAtUpperCase($string){
    return preg_replace('/(?<!\ )[A-Z]/', ' $0', $string);
}

function checkLongField($field) {
	$LongFields=array("RazonSocial", "DescripcionCampana", "ConceptoFactura", "Descripcion", "NombreCompleto", "Direccion");
	$superLongFields=array("ObservacionesAccion", "RangoCNAECorto");
	if(in_array($field, $LongFields)) return "min-width:200px;";
	if(in_array($field, $superLongFields)) return "min-width:350px;";



	else return "min-width:50px";
}


function getTableFromView($ViewName)
 {
 	if (strpos($ViewName, 'Contac') !== false) {
    return "PersonasContactoClientes";
	}
	if (strpos($ViewName, 'Accion') !== false) {
    return "AccionesSeguimientoOportunidades";
	}
	if (strpos($ViewName, 'ClientesCamp') !== false) {
    return "ClientesCampanasRel";
	}
	if (strpos($ViewName, 'Facturas') !== false) {
    return "Facturas";
	}
	if (strpos($ViewName, 'Grup') !== false) {
    return "GruposBonita";
	}
	if (strpos($ViewName, 'Memb') !== false) {
    return "MembresiasBonitaRel";
	}
	if (strpos($ViewName, 'Role') !== false) {
    return "RolesBonita";
	}
	if (strpos($ViewName, 'Usuar') !== false) {
    return "UsuariosBonita";
	}

	if (strpos($ViewName, 'Clientes') !== false) {
    return "Clientes";
	}
	if (strpos($ViewName, 'Oport') !== false) {
    return "Oportunidades";
	}
	if (strpos($ViewName, 'Campan') !== false) {
    return "Campanas";
	}


 }

function getIdFromTable($table) 
{
	include ('Mysqlconn.php'); //Sebastian
	$query="show columns from ARista.dbo.$table where 'Key' = 'PRI'";
	$List_sql = sqlsrv_query( $conexion, $query);

	while ($row = sqlsrv_fetch_array($List_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		$table=$row[0];		
	}

	return $table;


}
function check_state($table, $id, $value) 
{
	include ('Mysqlconn.php'); //Sebastian
	$query="select IdEstadoActividad from ARista.dbo." . $table . " where $id='" . $value . "'";
	$state_sql=sqlsrv_query( $conexion, $query);

	while ($row = sqlsrv_fetch_array($state_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		$state=$row[0];	
	}
	return $state;
}


function getColumnNames($ViewName) {
    
    include ("Mysqlconn.php");

	$querycolumns_names="SELECT (COLUMN_NAME) FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_CATALOG = 'ARista' AND TABLE_NAME = '$ViewName' AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%';";
    $columnNames_sql = sqlsrv_query( $conexion, $querycolumns_names);

    $columnNames = array();

    //if($ViewName=="OportunidadesView") array_push($columnNames, "IdOportunidad");

	while ($row = sqlsrv_fetch_array($columnNames_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
		array_push($columnNames, $row[0]);	
	}



	return $columnNames;
}

function filterColumns($columnNames) {
	$rcolumnNames=array();
  for($i=0;$i<count($columnNames);$i++) {
    if(strpos($columnNames[$i], 'Hito')!==false) continue;
    if(strpos($columnNames[$i], 'NumeroOportunidadesAbiertas')!==false) continue;
    if(strpos($columnNames[$i], 'NumeroClientesAsignados')!==false) continue;
    if(strpos($columnNames[$i], 'NumeroRespuestasPositivas')!==false) continue;
    if(strpos($columnNames[$i], 'ResultadoCampana')!==false) continue;
    else array_push($rcolumnNames, $columnNames[$i]);
  }
  return $rcolumnNames;
}

function getColumnNames_table($TableName) 
{
	include ("Mysqlconn.php");

	$querycolumns_names="SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_CATALOG = 'ARista' AND (TABLE_NAME = '$TableName') AND (ORDINAL_POSITION <> 1) AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT like 'IdEstadoActividad%' AND COLUMN_NAME NOT LIKE 'ssma\$rowid%'";

	$columnNames_sql = sqlsrv_query( $conexion, $querycolumns_names);
	
	if($columnNames_sql == false){
		echo( print_r( sqlsrv_errors(), true));
		die("$querycolumns_names");
	} 

    $columnNames = array();

	while ($row = sqlsrv_fetch_array($columnNames_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
		array_push($columnNames, $row[0]);		
	}

	return $columnNames;
}

function getColumnNamesString($columnNames) 
{
	$columnsString="";

	for($i=0;$i<sizeof($columnNames);$i++) 
	{
		$columnsString = $columnsString . $columnNames[$i] . ", ";
	}

	$columnsString = $columnsString . "IdEstadoActividad";

	//$columnsString = substr($columnsString, 0, -2);

	return $columnsString;
}
function getSumColumnNamesString($columnNamesAndTypes) 
{
	$numericType = array("int", "long", "smallint", "bigint", "float", "numeric", "double", "decimal");
	$columnsString="";
	foreach ($columnNamesAndTypes as $faKey => $nameAndType) {
		if(array_search($nameAndType["type"], $numericType )!== false){
			$columnsString.="SUM(".$nameAndType['name']."), ";
		}else{
			$columnsString.="null, ";
		}
	}

	// for($i=0;$i<sizeof($columnNames);$i++) 
	// {
	// 	$columnsString = $columnsString ."CASE WHEN ISNUMERIC(cast($columnNames[$i] as nvarchar)) = 1 THEN 'SUM(cast(cast($columnNames[$i] as nvarchar) as numeric))' ELSE null END as $columnNames[$i], ";
	// }

	$columnsString = $columnsString . "IdEstadoActividad";

	//$columnsString = substr($columnsString, 0, -2);

	return $columnsString;
}

function getOrder($ViewName) 
{
	switch ($ViewName) 
 	{
    	case "ClientesView":
        	return "CIFNIFNIE";
    	case 1:
        	echo "i equals 1";
        	break;
    	case 2:
        	echo "i equals 2";
        	break;
	}

}

function ID_to_cif($id) {
  include ('Mysqlconn.php'); //Sebastian
  $query="Select CIFNIFNIE from ARista.dbo.Clientes where IdClienteArista='$id'";
    $col_sql=sqlsrv_query( $conexion, $query);
    $col="";
    while($row = sqlsrv_fetch_array($col_sql, SQLSRV_FETCH_NUMERIC))
    {
      $col=$row[0];
    }
    return $col;
}

function IdOp_to_cif($id) {
  include ('Mysqlconn.php'); //Sebastian
  $query="Select CIFNIFNIE from ARista.dbo.Clientes where IdClienteArista=(select IdClienteArista from ARista.dbo.Oportunidades where IdOportunidad='$id')";
    $col_sql=sqlsrv_query( $conexion, $query);
    $col="";
    while($row = sqlsrv_fetch_array($col_sql, SQLSRV_FETCH_NUMERIC))
    {
      $col=$row[0];
    }
    return $col;
}

function getPK($ViewName) 
{
	include ('Mysqlconn.php');
	$table=getTableFromView($ViewName);
	// MODIFICAR: guion bajo (?)
	// $querydata="SHOW KEYS FROM ARista.dbo.$table where Key_name = 'PRIMARY'";
	$querydata = "SELECT CONSTRAINT_NAME FROM ARista.INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = '$table' AND CONSTRAINT_TYPE ='PRIMARY KEY'";
	$data_sql = sqlsrv_query( $conexion, $querydata);
	if($data_sql == false){
		echo( print_r( sqlsrv_errors(), true));
		die("Error to Get the $ViewName details.");
	} 
	$data=array();

	while ($row = sqlsrv_fetch_array($data_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		$data = $row;		
	}

	$PK=explode("_", $data[0])[2];

	return $PK;
}

function select_multi($query) 
{
	include ('Mysqlconn.php');
	$data_sql = sqlsrv_query( $conexion, $query);
	$data=array();
	if(empty($data_sql)) return "";

	while ($row = sqlsrv_fetch_array($data_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		array_push($data, $row);		
	}

	return $data;

}

function getData($columnNamesString, $ViewName)
{
	include ("Mysqlconn.php");
	
	$key=getPK($ViewName);
	$querydata="SELECT $key,".($columnNamesString)." FROM ARista.dbo.$ViewName where IdEstadoActividad='1'";
	$data_sql = sqlsrv_query( $conexion, $querydata);
	$data=array(array());

	while ($row = sqlsrv_fetch_array($data_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		array_push($data, $row);		
	}
	return $data;
}

function newRegisterButton($ViewName) 
{
	echo "<form action=\"InsertForm_EM.php\" method=\"post\">
	<input type=\"hidden\" name=\"action\" value=\"insert_no_action\"/>
	<input type=\"hidden\" name=\"ViewName\" value=\"$ViewName\"/>
	<input type=\"submit\" value=\"Nuevo registro\" class=\"homebutton\" id=\"newRegister\"/>
	</form>";
}

 function check($col_name)
{
	include ("Mysqlconn.php");
	//Select PK Names from SillyTable
	$querytables_names="SELECT TableName, PrimaryKey From ARista.dbo.SillyTable where PrimaryKeyValue='$col_name'";
	$row_sql = sqlsrv_query( $conexion, $querytables_names);
	
	$values = array();

	if(count($row_sql)==0) return "";

	while($row = sqlsrv_fetch_array($row_sql, SQLSRV_FETCH_NUMERIC))
		{
			array_push($values, $row[0]);
			array_push($values, $row[1]);
		}
	//$find_value = array();

	return $values;
}

function checkPK($col_name, $ViewName)
{
	include ("Mysqlconn.php"); //Sebastian
	//Select PK Names from SillyTable
	$querytables_names="SELECT TableName, Field From ARista.dbo.MaintainanceAux1 where Alias='$col_name'";
	$row_sql = sqlsrv_query( $conexion, $querytables_names);
	$values = array();

	while($row = sqlsrv_fetch_array($row_sql, SQLSRV_FETCH_NUMERIC))
		{
			array_push($values, $row[0]);
			array_push($values, $row[1]);
		}

	$querycheck="SELECT ReferencedTableName, ComboValue from ARista.dbo.ForeignKeysAux where ReferencedTableName='" . $values[0] . "' and ComboValue='" . $values[1] . "'";	
	$finalvalues_sql=sqlsrv_query( $conexion, $querycheck);
	$finalvalues=array();


	while($row = sqlsrv_fetch_array($finalvalues_sql, SQLSRV_FETCH_NUMERIC))
		{
			array_push($finalvalues, $row[0]);
			array_push($finalvalues, $row[1]);
		}

	if(count($finalvalues)==0) return "";


	//CIFNIFNIE CLIENTES
	if($finalvalues[0]=='Clientes' && $finalvalues[1]=='CIFNIFNIE') return "";
	return $values;
}

function checkPK_table($col_name, $TableName)
{
	include ("Mysqlconn.php"); //Sebastian
	$querycheck="SELECT ReferencedTableName, ComboValue from ARista.dbo.ForeignKeysAux where TableName='" . $TableName . "' and ColumnName='" . $col_name . "'";	
	$finalvalues_sql=sqlsrv_query( $conexion, $querycheck);
	$finalvalues=array();

	while($row = sqlsrv_fetch_array($finalvalues_sql, SQLSRV_FETCH_NUMERIC))
		{
			array_push($finalvalues, $row[0]);
			array_push($finalvalues, $row[1]);
		}

	if(count($finalvalues)==0) return "";


	//CIFNIFNIE CLIENTES
	return $finalvalues;
}


function convert_to_id($field) 
{
	include ("Mysqlconn.php");
	$query = "SELECT ColumnName FROM ARista.dbo.ForeignKeysAux where ComboValue='$field'";
	$row_sql=sqlsrv_query( $conexion, $query);


	while ($row = sqlsrv_fetch_array($row_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		$id_value=$row[0];		
	}

	if(empty($id_value)) return $field;

	return $id_value;
}

function createSelect($values, $col_name, $table) 
{
	$createSelectHtml ="";
	include ("Mysqlconn.php");
	$query = "SELECT " . $values[1] . " FROM ARista.dbo." . $values[0] . " order by " . $values[1];

	$row_sql=sqlsrv_query( $conexion, $query);
	$select_array = array();

	while ($row = sqlsrv_fetch_array($row_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		array_push($select_array, $row[0]);		
	}
	if(strpos($col_name, 'Id')!==false) $col_name2 = substr($col_name, 2);
	else $col_name2=$col_name;

	$mand=check_mandatory($col_name, $table);
	if($mand==1) {
		$star='*';
		$required="class=\"required\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\"";
		$requiredDiv="name=\"requiredSelectDiv\"";
	}
	else {
		$star='';
		$required="";
		$requiredDiv="";
	}

	$createSelectHtml.= "<div class=\"input-field col s12\"" . $requiredDiv . ">
			<select id=\"$table" . "_$col_name\" style=\"width: 400px\" name=\"" . htmlspecialchars($col_name) . "\" " . $required . ">
			//$values[1]
				<option value=\"\" disabled selected>" . $col_name2 . "</option>";
	for($i=0;$i<count($select_array);$i++) 
	{
		$createSelectHtml.= "<option>$select_array[$i]</option>";
	}
	$createSelectHtml.= "</select>";
			$createSelectHtml.= "<label>" . $col_name2 . "$star</label>";
	$createSelectHtml.= "</div>";
	$createSelectHtml.=  "<br>";
	$createSelectHtml.=  "<br>";

	$createSelectHtml.= initialize_select("$table" . "_$col_name");
	
	return $createSelectHtml;

}

/*function createInsertForm($columnNames, $TableName) 
{
	echo "<fieldset>
    			<form class=\"col s12\" method=\"POST\" action=\"InsertCode_EM.php\">
    				<b align=\"left\">Añadir nuevos datos</b>
    				<br><br>";
    echo "<input type =\"hidden\" name=\"TableName\" value=\"$TableName\"/>";
	
	for($i=0;$i<count($columnNames);$i++) 
	{
		$values=checkPK_table($columnNames[$i], $TableName);
		if(empty($values)) 
		{
			createSimpleField($columnNames[$i]);
		}
		else 
		{
			createSelect($values);
		}

	}
	echo "<input type=\"submit\" value=\"Añadir registro\" id=\"Insert\"/>";
	echo "</form>
			</fieldset>";
}*/

function FieldTypetoType($fieldType) {


	if(strpos(strtolower($fieldType), "int")!==false || strpos(strtolower($fieldType), "decimal")!==false || strpos(strtolower($fieldType), "double")!==false) return "\"text\" onkeyup=\"digitsOnly(this);\"";

	return "text";
}

function sessionTimeout()
{
	$timeout = 1800; // Number of seconds until it times out.
 
// Check if the timeout field exists.
if(isset($_SESSION['timeout'])) {
    // See if the number of seconds since the last
    // visit is larger than the timeout period.
    $duration = time() - (int)$_SESSION['timeout'];
    if($duration > $timeout) {
        // Destroy the session and restart it.
        session_destroy();
        session_start();
        return "TIMEOUT";
    }
}
 
// Update the timeout field with the current time.
$_SESSION['timeout'] = time();
return "";

}



function createSimpleField($FieldName, $table) 
{
	$createSimpleFieldHtml ="";
	
	$mand=check_mandatory($FieldName, $table);
	$fieldType=getFieldType($table, $FieldName);
/*	echo "<script type='text/javascript'>alert('$table');</script>";
	echo "<script type='text/javascript'>alert('$FieldName');</script>";
	echo "<script type='text/javascript'>alert('$fieldType');</script>";*/
	$type=FieldTypeToType($fieldType);
	//echo "<script type='text/javascript'>alert('$type');</script>";
	if($mand==1) $star='*';
	else $star='';
	if (strpos($FieldName, 'Fecha') !== false) 
	{
		$createSimpleFieldHtml.= "<div class=\"row\">
        		<div class=\"input-field col s6\">";
        		if($mand==1) {
        			$createSimpleFieldHtml.= "<input id=\"$FieldName\" type=\"text\" required=\"\" aria-required=\"true\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\" class=\"datepicker\" name=\"$FieldName\"/>";
        		}
        		else {
        			$createSimpleFieldHtml.= "<input id=\"$FieldName\" type=\"text\" class=\"datepicker\" name=\"$FieldName\"/>";
        		}
          				
          			$createSimpleFieldHtml.= "<label for=\"$FieldName\">$FieldName$star</label>     				
        		</div>
      		 </div>";
	}

	else 
	{	
      	$createSimpleFieldHtml.= "<div class=\"row\">
        		<div class=\"input-field col s6\">";
        		if($mand==1) {
        			$createSimpleFieldHtml.= "<input id=\"$FieldName\" type=" . $type . " required=\"\" aria-required=\"true\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\" class=\"validate\" name=\"$FieldName\"/>";
        		}
        		else {
        			$createSimpleFieldHtml.= "<input id=\"$FieldName\" type=" . $type . " class=\"validate\" name=\"$FieldName\"/>";
        		}
          				
          		$createSimpleFieldHtml.= "<label for=\"$FieldName\">$FieldName$star</label>
        		</div>
      		 </div> 
      		 ";
	}
	return $createSimpleFieldHtml;
}

function initialize_select($id) 
{
	return "<script type=\"text/javascript\"> 
		$(document).ready(function() {
    		$('#$id').material_select();
  		});
  		</script>";
}

function getViewFromTable($table) 
{
	switch ($table) 
	{
		case 'Clientes':
		return "ClientesView";
		break;

		case 'ClientesDatosOperativosDet':
		return "ClientesView";
		break;

		case 'ClientesDatosEconomicosDet':
		return "ClientesView";
		break;

		case 'ClientesConfiguracionesOperativasDet':
		return "ClientesView";
		break;

		case 'Facturas':
		return "FacturasView";
		break;

		case 'FacturasEvolucionDet':
		return "FacturasView";
		break;

		case 'AccionesSeguimientoOportunidades':
		return "AccionesView";
		break;

		case 'Oportunidades':
		return "OportunidadesView";
		break;

		case 'Campanas':
		return "CampanasView";
		break;

		case 'RolesBonita':
		return "RolesView";
		break;


	}


}
function getColumnNamesAndTypes($ViewName){

    include ("Mysqlconn.php");

	$querycolumns_names="SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_CATALOG = 'ARista' AND TABLE_SCHEMA = 'dbo' AND TABLE_NAME = '$ViewName' AND COLUMN_NAME NOT LIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%';";
    $columnNames_sql =sqlsrv_query($conexion, $querycolumns_names);

	$columnNames = array();

    //if($ViewName=="OportunidadesView") array_push($columnNames, "IdOportunidad");

	while ($row = sqlsrv_fetch_array($columnNames_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
		array_push($columnNames, $row[0]);	
	}


    $query="SELECT DATA_TYPE FROM information_schema.COLUMNS where TABLE_SCHEMA = 'dbo' AND TABLE_CATALOG='ARista' AND TABLE_NAME = '$ViewName' AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%'";
    //  DATA_TYPE FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_CATALOG = 'ARista' AND TABLE_NAME = '$View' AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%'
	$Views_sql = sqlsrv_query( $conexion, $query);
	if( !$Views_sql )
{
	 echo ( print_r( sqlsrv_errors(), true));
}
    $types=array();

	while ($row = sqlsrv_fetch_array($Views_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		array_push($types,$row[0]);	
    }
    $columns = array();

    foreach ($columnNames as $i => $name) {
        array_push($columns, ["name" => $name, "type" => $types[$i], "width" => checkLongField($name)]);
    }
    return $columns;
    


}
function createAndHideTables($Rol) {

    if(empty($Rol)) {
        //displayLogInErrorMessage();
        redirectToLogIn();
        return;
    }

    //cambiar a tables
    $ViewList=retrieveViewList();

    $tables = array();

    for($i=0;$i<sizeof($ViewList);$i++) {
        
        array_push($tables, ["name"=>$ViewList[$i], "columns"=>getColumnNamesAndTypes($ViewList[$i])]);
    }
    $tablesResponse = array("tables" => $tables);
    $tablesJson = json_encode($tablesResponse);
    return $tablesJson;
}

function getDataTable($View, $Rol){
    include ("Mysqlconn.php");
	$columnNames = getColumnNames($View);
	$columnNamesAndTypes = getColumnNamesAndTypes($View);
	$columnNamesString = getColumnNamesString($columnNames);
	$columnSumNameString = getSumColumnNamesString($columnNamesAndTypes);
    $table_name = getTableFromView($View);
	
	$key=getPK($View);
    $params = $cols = $totalRecords = $data = $totalSum= array();

    $params = $_REQUEST;
    $cols = $columnNames;
		$colsFilteredNumber;
		$colsFilteredSearch;
	foreach ($params["columns"] as $i => $column) {
		if(!empty($column["search"]["value"])){
			$colsFilteredNumber[] = $i;
			$colsFilteredSearch[] = $column["search"]["value"];
		}
	}

	$where_condition = $sqlTot = $sqlRec = $sqlSumTotal = "";

	if(!empty($params['search']['value'])){
		$where_condition .= "AND ( ";
		foreach ($columnNames as $i => $column) {
			if($i == 0 ){
				$where_condition .= "$column LIKE '%".$params['search']['value']."%' ";
			}else{
				$where_condition.= "OR $column LIKE '%".$params['search']['value']."%' ";
			}
			if($i == (count($columnNames)-1)){
				$where_condition.= ")";
			}
		}
	}
	if(isset($colsFilteredNumber)){
		//for every column that has a filter on we get their number
		foreach ($colsFilteredNumber as $ikey => $index) {
			//Grab the value of that column search and separate the range filter and column filter with |
			$searchValueArray = explode("|", $colsFilteredSearch[$ikey]);
			foreach ($searchValueArray as $key2 => $filterSearch) {
				if($filterSearch !=""){
					//do the query depending if it is a range filter or a column search
					if(strpos($filterSearch, "Range;") !== false){
						$where_condition.= "AND ( ".$cols[$index];
						//separate the min and max range
						$rangeQueryArray= explode(";", $filterSearch);
						//query for range
						if($rangeQueryArray[3]=="DATE"){
							$where_condition.=" BETWEEN '".$rangeQueryArray[1]."' AND '".$rangeQueryArray[2]."' ) ";
						}else{
							$where_condition.=" BETWEEN ".$rangeQueryArray[1]." AND ".$rangeQueryArray[2]." ) ";
						}
					}else{
						//query for column filter
						$where_condition.= "AND ( ".$cols[$index]." LIKE '%".$filterSearch."%' ) ";
					}
				}
			}

		}

	}

	$sql_query = "SELECT $key,".($columnNamesString)." FROM ARista.dbo.$View where IdEstadoActividad='1'";
	$sql_query_sum_total = "SELECT null, ".($columnSumNameString)." FROM ARista.dbo.$View where IdEstadoActividad='1' GROUP BY IdEstadoActividad";
	// foreach ($columnNames as $faKey => $colName) {
	// 	$sql_query_sum_total.=", $colName";
	// }

	$sqlTot.=$sql_query;
	$sqlRec.=$sql_query;
	$sqlSumTotal.=$sql_query_sum_total;


	if(isset($where_condition) && $where_condition != ''){
		$sqlTot.= $where_condition;
		$sqlRec.= $where_condition;
		// uncomment if you want the totals to be filtered too
		// $sqlSumTotal.= $where_condition;
	}

	$orderBy = $params['order'][0]['column'];
	//when in views that have actions, an extra column is added at the beggining, shifting the whole ennumeration by 1
	if(!in_array($View, array("FacturasView", "GruposView", "RolesView", "UsuariosView", "MembresiasView")) && strcmp($Rol,"admin")===0 && $orderBy!=00)
	{
		$orderBy--;
	}

	if($params['length'] != -1){
		$sqlRec.= " ORDER BY ".$cols[$orderBy]." ".$params['order'][0]['dir']." OFFSET "
		.$params['start']." ROWS FETCH NEXT ".$params['length']." ROWS ONLY";
	}else{
		$sqlRec.= " ORDER BY ".$cols[$orderBy]." ".$params['order'][0]['dir'];
	}

	// $queryTot = mysqli_query($conexion, $sqlTot)or die("Database error: ". mysqli_error($conexion)." \nsql: ".$sqlTot);
	$queryTot = sqlsrv_query($conexion, $sqlTot,array(), array( "Scrollable" => 'keyset' ));
	if($queryTot == false){
		echo "error here";
		die( print_r( sqlsrv_errors(), true));
	} 
	$totalRecords = sqlsrv_num_rows($queryTot);
	if ($totalRecords === false){
		die( print_r( sqlsrv_errors(), true));
		echo "Error in retrieveing row count.";
	}


	$queryRecords = sqlsrv_query($conexion, $sqlRec);
	if($queryRecords == false){
		echo($sqlRec);
		die( print_r( sqlsrv_errors(), true));
	} 
	$querySumTotals = sqlsrv_query($conexion, $sqlSumTotal);
	if($querySumTotals == false){
		echo($sqlSumTotal);
		die( print_r( sqlsrv_errors(), true));
	} 
	while($row = sqlsrv_fetch_array($querySumTotals,SQLSRV_FETCH_NUMERIC )){
		array_splice($row,0, 1);
		$totalSum[] = $row;
	}
	// echo json_encode($totalSum);

	while( $row = sqlsrv_fetch_array($queryRecords, SQLSRV_FETCH_NUMERIC)){
		// REMOVED WHEN DATETIME FIXED
		foreach($row as $faKey => $faValue){
			if($faValue instanceof DateTime){
				$row[$faKey] = $faValue->format("Y-m-d");
			}
		}
		// ALO REMOVED
		$row=utf8ize($row);
		if(!in_array($View, array("FacturasView", "GruposView", "RolesView", "UsuariosView", "MembresiasView")) && strcmp($Rol,"admin")===0)
		{
			if($row[count($row)-1] == '0') {
				$row[0]= "<td style=\"width:40px;\"> <a href=\"./Form.php?Id=$key" . "_" . $row[0] . "&ViewName=$View\"><i class=\"material-icons\">edit</i></a>	
				<a href=\"./DeleteCode.php?Id=$table_name" . "_" . "$key" . "_" . $row[0] . "&ActOrInact=Act\" onclick=\"return confirm('Está seguro de que quiere borrar este registro?')\"> <i class=\"material-icons\">replay</i></a></td>";
			}
			else {
				$row[0]= "<td style=\"text-align:center;width:40px;\"> <a href=\"./Form.php?Id=$key" . "_" . $row[0] . "&ViewName=$View\"> <i class=\"material-icons\">edit</i> </a>	
				<a href=\"./DeleteCode.php?Id=$table_name" . "_" . "$key" . "_" . $row[0] . "&ActOrInact=Inact\" onclick=\"return confirm('Está seguro de que quiere borrar este registro?')\"> <i class=\"material-icons\">delete</i></a></td>";
			}
		}else{
			array_splice($row,0, 1);
		}
		$data[] = $row;
		}
	$json_data = array(
		"draw"          =>intval($params['draw']),
		"recordsTotal"  =>intval($totalRecords),
		"recordsFiltered" => intval($totalRecords),
		"data"          =>$data,
		"totals"		=>$totalSum,
		"query"			=>$sqlRec,
		"table"			=>$View,
		"key"			=>$key,
		"sum"			=>$sqlSumTotal
	);
    return json_encode($json_data);

        
	}
	// to help sanitize jsons
	function utf8ize($d) {
		if (is_array($d)) {
			foreach ($d as $k => $v) {
				$d[$k] = utf8ize($v);
			}
		} else if (is_string ($d)) {
			return utf8_encode($d);
		}
		return $d;
	}
	
	// ---------------- FORM SCRIPTS -----------------------

	function createTabsAndForms($ViewName, $action, $id, $Rol) 
{
	if(empty($Rol)) {
		redirectToLogIn();
		return;
	}
	
	$tables=getTablesfromView($ViewName);
	// $tables = array();

	// foreach (getTablesfromView($ViewName) as $key => $tableName) {
	// 	array_push($tables, ["name"=> $tableName, ]);
	// }

	$responseHtml="";
	$responseHtml.= "<div class=\"row\">
    			<div class=\"col s12\">
      				<ul class=\"tabs\">";
	for($i=0;$i<sizeof($tables);$i++) 
	{
		$responseHtml.= "<li class=\"tab col s3\"><a href=\"#$tables[$i]\">$tables[$i]</a></li>";
	}
	$responseHtml.= "</ul></div>";

	for($i=0;$i<sizeof($tables);$i++) 
	{
		$responseHtml.= "<div id=\"$tables[$i]\" class=\"col s12\">";

		$responseHtml.= createForm($tables[$i], $action, $id);

		$responseHtml.= "</div>";

		//FormJQuery($tables[$i]);
	}
	$responseHtml.= "</div>";

	$responseHtml.= "<script type=\"text/javascript\">
			$(document).ready(function(){
    				$('ul.tabs').tabs();
  			});
  			</script>";

	return utf8_encode( $responseHtml);

}

function FormJQuery($table) {

	$columnNames=getColumnNames_table($table);

	echo "<script type=\"text/javascript\">";
	echo "$(\"#$table\").validate({
        rules: {";
        $var=0;

    for($i=0;$i<sizeof($columnNames);$i++) {
    	$check=check_mandatory($columnNames[$i], $table);
    	if($check==0) continue;
    	else {
    		if($var==0) {
    			echo "$table" . "_$columnNames[$i]: {
    			required: true,
    		}
    		";
    		$var=1;
    		}
    		else {
    			echo ",
    			$table" . "_$columnNames[$i]: {
    			required: true,
    		}";

    		}
    		
    	}

    }
    echo ");</script>";

}

function check_mandatory($field, $table) {
	include('Mysqlconn.php');

	$query="SELECT IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$table' AND COLUMN_NAME='$field'";
	$row_sql=sqlsrv_query( $conexion, $query);

	while ($row = sqlsrv_fetch_array($row_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		$bool=$row[0];	
	}
	if($bool=='YES') return 0;
	if($bool=='NO') return 1;
}

function getTablesfromView($ViewName) 
{
	$tables=array();
	switch ($ViewName) 
 	{
    	case "ClientesView":
    		array_push($tables, "Clientes");
    		array_push($tables, "ClientesConfiguracionesOperativasDet");
    		array_push($tables, "ClientesDatosEconomicosDet");
    		array_push($tables, "ClientesDatosOperativosDet");
        break;

    	case "OportunidadesView":
        	array_push($tables, "Oportunidades");
        	break;

    	case "CampanasView":
        	array_push($tables, "Campanas");
        	break;
        case "AccionesView":
        	array_push($tables, "AccionesSeguimientoOportunidades");
        	break;
		
		case "ContactosView":
			array_push($tables, "PersonasContactoClientes");
			break;

		case "FacturasView":
			array_push($tables, "Facturas");
			array_push($tables, "FacturasEvolucionDet");
			break;

		case "ClientesCampanasView":
			array_push($tables, "ClientesCampanasRel");
			break;

		case "GruposView":
			array_push($tables, "GruposBonita");
			break;

		case "MembresiasView":
			array_push($tables, "MembresiasBonitaRel");
			break;

		case "RolesView":
			array_push($tables, "RolesBonita");
			break;

		case "UsuariosView":
			array_push($tables, "UsuariosBonita");
		
	}

	return $tables;
}

function createForm($table, $action, $id) 
{
	$createFormHtml ="";
	if($action==0) 
	{
		$createFormHtml.= createInsertForm($table);
	}
	if($action==1) 
	{
		$createFormHtml.= createEditForm($table, $id);
	}
	return $createFormHtml;
}

function createInsertForm($table) 
{
	$createInsertFormHtml ="";

	$columnNames=getColumnNames_table($table);

	$rcolumnNames=filterColumns($columnNames);

	$createInsertFormHtml.= "<fieldset>
    			<form class=\"col s12\" method=\"POST\" action=\"./InsertCode_EM.php\" id=\"$table\">
    				
    				<br><br>";
    $createInsertFormHtml.= "<div class=\"row\">
  			<div class=\"column\">";

    $createInsertFormHtml.= "<input type =\"hidden\" name=\"TableName\" value=\"$table\"/>";
    $createInsertFormHtml.= "<input type =\"hidden\" name=\"mysql\" value=\"insert\"/>";
	
	for($i=0;$i<count($rcolumnNames)/2;$i++) 
	{
		$values=checkPK_table($rcolumnNames[$i], $table);
		if(empty($values)) 
		{
			$createInsertFormHtml.= createSimpleField($rcolumnNames[$i], $table);
		}
		else 
		{
			$createInsertFormHtml.= createSelect($values, $rcolumnNames[$i], $table);
		}

	}

	$createInsertFormHtml.= "</div>";
	$createInsertFormHtml.= "<div class=\"column\">";


	for($j=$i;$j<count($rcolumnNames);$j++) 
	{
		$values=checkPK_table($rcolumnNames[$j], $table);
		if(empty($values)) 
		{
			$createInsertFormHtml.= createSimpleField($rcolumnNames[$j], $table);
		}
		else 
		{
			$createInsertFormHtml.= createSelect($values, $rcolumnNames[$j], $table);
		}


	}
	$createInsertFormHtml.= "</div>";
	$createInsertFormHtml.= "</div>";
	$createInsertFormHtml.= createFormButtons($table, "Insert");
	
	$createInsertFormHtml.= "</form>
			</fieldset>";
	return $createInsertFormHtml;
}

function createFormButtons($table, $action) 
{
	$createFormButtonsHtml ="";
	if($action=="Insert") {
		$createFormButtonsHtml.= "<button class=\"btn waves-effect waves-light pulse\" type=\"submit\" name=\"action\" onclick=\"return checkMandatorySelects(this.form)\">Guardar registro<i class=\"material-icons right\">cloud</i></button>";
		
		//previous button anadir registro
		//onclick=\"return checkMandatorySelects(this.form)\"
		//"<input type=\"submit\" value=\"Añadir registro\" />";
		$createFormButtonsHtml.= "<button type=\"button\" class=\"btn waves-effect waves-light\" onclick=\"window.location.href='./EFirstPage.php'\"><i class=\"material-icons right\">home</i>Volver a la tabla</button>";
		
		$createFormButtonsHtml.= "<button type=\"reset\" class=\"btn waves-effect waves-light\"><i class=\"material-icons right\">autorenew</i>Limpiar</button>";
	}

	if($action=="Update") {
		$createFormButtonsHtml.= "<button class=\"btn waves-effect waves-light\" type=\"submit\" name=\"action\"><i class=\"material-icons right\" onclick=\"return checkMandatorySelects(this.form)\">send</i>Actualizar registro</button>";
		$createFormButtonsHtml.= "<a class=\"btn waves-effect waves-light\" onclick=\"window.location.href='./EFirstPage.php'\"><i class=\"material-icons right\">home</i>Volver a la tabla</a>";
		$createFormButtonsHtml.= "<button type=\"reset\" class=\"btn waves-effect waves-light\"><i class=\"material-icons right\">autorenew</i>Valores iniciales</button>";
	}
	return $createFormButtonsHtml;

}

//confirm('Está seguro de que quiere editar este registro?')

function createEditForm($table, $id) 
{
	$createEditFormHtml ="";
	$id_value = explode("_", $id);

	$data=getDataEdit($table, $id_value[0], $id_value[1]);
	if(empty($data)) {
		$createEditFormHtml.= "No se han registrado datos todavía. Para añadir datos, volver a la tabla y clicar en nuevo registro" . "<br>";
		return;
	}
	$columnNames=getColumnNames_table($table);
	$rcolumnNames=array();
	for($i=0;$i<count($columnNames);$i++) {
		if(strpos($columnNames[$i], 'Hito')!==false) continue;
		else array_push($rcolumnNames, $columnNames[$i]);
	}

	$createEditFormHtml.= "<fieldset>
    			<form class=\"col s12\" method=\"POST\" action=\"./UpdateCode_EM.php\" id=\"$table\">
    				<br><br>";
    $createEditFormHtml.= "<div class=\"row\">
  			<div class=\"column\">";

    $createEditFormHtml.= "<input type =\"hidden\" name=\"TableName\" value=\"$table\"/>";
    $createEditFormHtml.= "<input type =\"hidden\" name=\"Id\" value=\"".htmlspecialchars($id_value[0]). "_" . "$id_value[1]\"/>";
	
	for($i=0;$i<count($rcolumnNames)/2;$i++) 
	{
		$values=checkPK_table($rcolumnNames[$i], $table);
		if(empty($values)) 
		{
			$createEditFormHtml.= createSimpleField_edit($rcolumnNames[$i], $data[0][$i], $table);
		}
		else 
		{
			$createEditFormHtml.= createSelect_edit($values, $rcolumnNames[$i], $table, $data[0][$i]);
		}

	}

	$createEditFormHtml.= "</div>";
	$createEditFormHtml.= "<div class=\"column\">";


	for($j=$i;$j<count($rcolumnNames);$j++) 
	{
		$values=checkPK_table($rcolumnNames[$j], $table);
		if(empty($values)) 
		{
			$createEditFormHtml.= createSimpleField_edit($rcolumnNames[$j], $data[0][$j], $table);
		}
		else 
		{
			$createEditFormHtml.= createSelect_edit($values, $rcolumnNames[$j], $table, $data[0][$j]);
		}
	}

	$createEditFormHtml.= "</div>";
	$createEditFormHtml.= "</div>";
	$createEditFormHtml.= createFormButtons($table, "Update");
	
	$createEditFormHtml.= "</form>
			</fieldset>";
	
	return $createEditFormHtml;

}

function createSelect_edit($values, $col_name, $table, $value) 
{
	$createSelect_edit="";
	include ("Mysqlconn.php");
	$query = "SELECT " . $values[1] . " FROM ARista.dbo." . $values[0] . " order by " . $values[1];
	$row_sql=sqlsrv_query( $conexion, $query);
	$select_array = array();

	while ($row = sqlsrv_fetch_array($row_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		array_push($select_array, $row[0]);		
	}
	if(strpos($col_name, 'Id')!==false) $col_name2 = substr($col_name, 2);
	else $col_name2=$col_name;


	//Get Id name

	$query2 = "SELECT ReferencedColumnName FROM ARista.dbo.ForeignKeysAux where ComboValue='"  . $values[1] . "' and TableName='$table'";
	$row_sql=sqlsrv_query( $conexion, $query2);


	while ($row = sqlsrv_fetch_array($row_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		$id_name=$row[0];		
	}


	$query3 = "SELECT " . $values[1] . " FROM ARista.dbo." . $values[0] . " where " . $id_name . "='" . $value . "'";
	if($value != ''){
		$row_sql=sqlsrv_query( $conexion, $query3);
		if($row_sql == false){
			echo (print_r( sqlsrv_errors(), true));
		}
	
		while ($row = sqlsrv_fetch_array($row_sql, SQLSRV_FETCH_NUMERIC)) 
		{
			if(empty($row[0])) $value_select="";
			else {
				$value_select=$row[0];
			}
					
		}
	}else{
		$value_select=$value;
	}


	if(empty($value_select)) $value_select="";
	$mand=check_mandatory($col_name, $table);
	if($mand==1) {
		$star='*';
		$required="required=\"\" aria-required=\"true\"";
		$requiredDiv="name=\"requiredSelectDiv\"";
	}
	else {
		$star='';
		$required="";
		$requiredDiv="";
	}

	$createSelect_edit.= "<div class=\"input-field col s12\"" . $requiredDiv . ">
			<select id=\"$table" . "_$col_name\" style=\"width: 400px\" name=\"" . $values[1] . "\"" . $required . ">
				<option value=\" \" disabled selected>" . $col_name2 . "</option>";
	for($i=0;$i<count($select_array);$i++) 
	{
		if($select_array[$i]==$value_select) {
			$createSelect_edit.= "<option selected=\"selected\">".htmlspecialchars($select_array[$i])."</option>";
		}
		else {
			$createSelect_edit.= "<option>".htmlspecialchars($select_array[$i])."</option>";
		}
		
	}
	$createSelect_edit.= "</select>";
	$createSelect_edit.= "<label>" . $col_name2 . $star . "</label>";
	$createSelect_edit.= "</div>";
	$createSelect_edit.=  "<br>";

	$createSelect_edit.=initialize_select("$table" . "_$col_name");
	
	return $createSelect_edit;
}



function createSimpleField_edit($FieldName, $value, $table) 
{
	$createSimpleField_editHtml="";
	if(isset($value)){
		$active="active";
	}else{
		$active="";
	}
/*	if (strpos($FieldName, 'Fecha') !== false) 
	{
		echo "<div class=\"row\">
        		<div class=\"input-field col s6\">
          				<input id=\"$FieldName\" type=\"text\" class=\"datepicker\" name=\"$FieldName\" value=\"$value\"/>
          				<label for=\"$FieldName\">$FieldName</label>
        		</div>
      		 </div> 
      		 <br>";
	}

	else 
	{	
      	echo "<div class=\"row\">
        		<div class=\"input-field col s6\">
          				<input id=\"$FieldName\" type=\"text\" class=\"validate\" name=\"$FieldName\" value=\"$value\"/>
          				<label for=\"$FieldName\">$FieldName</label>
        		</div>
      		 </div> 
      		 <br>";
	}*/
	$fieldType=getFieldType($table, $FieldName);
	$type=FieldTypeToType($fieldType);


	$mand=check_mandatory($FieldName, $table);
	if($mand==1) $star='*';
	else $star='';
	if (strpos($FieldName, 'Fecha') !== false) 
	{
		if($value instanceof DateTime){
			$value = $value->format('Y-m-d');
		}
		$createSimpleField_editHtml.= "<div class=\"row\">
        		<div class=\"input-field col s6\">";
        		if($mand==1) {
        			$createSimpleField_editHtml.= "<input id=\"$FieldName\" type=\"text\" required=\"\" aria-required=\"true\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\" class=\"datepicker\" name=\"$FieldName\" value=\"".htmlspecialchars($value)."\"/>";
        		}
        		else {
        			$createSimpleField_editHtml.= "<input id=\"$FieldName\" type=\"text\" class=\"datepicker\" name=\"$FieldName\" value=\"".htmlspecialchars($value)."\"/>";
        		}
          			$createSimpleField_editHtml.= "<label class=\"$active\"  for=\"$FieldName\">$FieldName$star</label>     				
        		</div>
      		 </div>";
	}

	else 
	{	
      	$createSimpleField_editHtml.= "<div class=\"row\">
        		<div class=\"input-field col s6\">";
        		if($mand==1) {
        			$createSimpleField_editHtml.= "<input id=\"$FieldName\" type=" . $type . " required=\"\" aria-required=\"true\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\" class=\"validate\" name=\"$FieldName\" value=\"".htmlspecialchars($value)."\"/>";
        		}
        		else {
        			$createSimpleField_editHtml.= "<input id=\"$FieldName\" type=" . $type . " class=\"validate\" name=\"$FieldName\" value=\"".htmlspecialchars($value)."\"/>";
        		}
          				
          		$createSimpleField_editHtml.= "<label class=\"$active\" for=\"$FieldName\">$FieldName$star</label>
        		</div>
      		 </div> 
      		 ";
	}
	return $createSimpleField_editHtml;
	
	
}

function getDataEdit($table, $pk, $pk_value) 
{
	$columnNames=getColumnNames_table($table);

	 $columnNamesString=getColumnNamesString($columnNames);
	 

	$query="SELECT ".($columnNamesString)." FROM $table where $pk='$pk_value'";
	$data=select_multi($query);

	return $data;
}

function displaycard($ViewName, $cif, $idoportunidad) 
{
	switch($ViewName) {
		case "OportunidadesView":
		echo "<div class=\"row\">
        		<div class=\"col s12 m6\">
          			<div class=\"card blue-grey darken-1\">
            			<div class=\"card-content white-text\">
              				<span class=\"card-title\">Información del cliente</span>
              				<p>CIFNIFNIE: $cif.</p>
            			</div>
          			</div>
        		</div>
      		</div>";
      	break;
      	case "AccionesView":
      	$cif=IdOp_to_cif($idoportunidad);
      	echo "<div class=\"row\">
        		<div class=\"col s12 m6\">
          			<div class=\"card blue-grey darken-1\">
            			<div class=\"card-content white-text\">
              				<span class=\"card-title\">Información del cliente</span>
              				<p>CIFNIFNIE: $cif.</p>
              				<p>IdOperacion: $idoportunidad.</p>
            			</div>
          			</div>
        		</div>
      		</div>";
      	break;
      	default:
      	echo "";
	}

}

function createFormHeader($ViewName, $action, $Rol) {
	if(empty($Rol)) return;
	$View = str_replace("View","", $ViewName);
	if($action==0) echo "<header id=\"BillibHeader\"><h2 align=\"center\">$View: Nuevo registro</h2></header>";
	if($action==1) echo "<header id=\"BillibHeader\"><h2 align=\"center\">$View: Editar registro</h2></header>";
}




?>