<?php

function createAndHideTables() {

    $ViewList=retrieveViewList();
    for($i=0;$i<sizeof($ViewList);$i++) {
    	echo "<div style=\"display:none\" id=\"div_$ViewList[$i]\">";
		
    	echo "<table id=\"$ViewList[$i]\" class=\"display\" cellspacing=\"0\" width=\"100%\">";        
        $columnNames=getColumnNames($ViewList[$i]);
        createHeaders($columnNames);
        $columnNamesString=getColumnNamesString($columnNames);
        $data=getData($columnNamesString, $ViewList[$i]);
        displaydata($data, $ViewList[$i]); 
        echo "</table>";
        echo "</div>";
        
    }   
    
}

?>

<html>
<head>
<style>
* {
    box-sizing: border-box;
}

body {
    font-family: Arial;
    padding: 20px;
    background: #f1f1f1;
}

/* Header/Blog Title */
.header {
    padding: 30px;
    font-size: 40px;
    text-align: center;
    background: white;
}

/* Style the top navigation bar */
.topnav {
    overflow: hidden;
    background-color: #333;
}

/* Style the topnav links */
.topnav a {
    float: left;
    display: block;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

/* Change color on hover */
.topnav a:hover {
    background-color: #ddd;
    color: black;
}

/* Create two unequal columns that floats next to each other */
/* Left column */
.leftcolumn {   
    float: left;
    width: 5%;
}

/* Right column */
.rightcolumn {
    float: left;
    width: 25%;
    background-color: #f1f1f1;
    padding-left: 20px;
}

/* Fake image */
.fakeimg {
    background-color: #aaa;
    width: 100%;
    padding: 20px;
}

/* Add a card effect for articles */
.card {
     background-color: white;
     padding: 20px;
     margin-top: 20px;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Footer */
.footer {
    padding: 20px;
    text-align: center;
    background: #ddd;
    margin-top: 20px;
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
@media (max-width: 800px) {
    .leftcolumn, .rightcolumn {   
        width: 100%;
        padding: 0;
    }
}

/* Responsive layout - when the screen is less than 400px wide, make the navigation links stack on top of each other instead of next to each other */
@media (max-width: 400px) {
    .topnav a {
        float: none;
        width:100%;
    }
}
</style>

<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Billib ARista</title>
        <link href="css/bootstrap.min.css" rel="stylesheet"/>
        <link href="css/bootstrap-theme.css" rel="stylesheet"/>
        <link rel="stylesheet" href="style/FirstPage.css"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link href="css/jquery.dataTables.min.css" rel="stylesheet"/>
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="materialize/js/materialize.min.js"></script>
</head>
<body>

<div class="header" id="BillibHeader"></header>


<div class="row">
	<div class="col-3 left">
		<div id="nav1">
			<div id="nav2">
				<?php
					include("functions_EM.php");
					$ViewList = retrieveViewList();
					createLeftBar($ViewList);
				?>
			</div>
		</div>
	</div>
	
	<div class="mainTable">
	<?php
        createAndHideTables();
     ?>

	</div>
</div>

<div id="BillibFooter" class="footer">
  <center><a href="http://proceedit.blogspot.com.es/">Copyright © 2018 Proceedit, all rights reserved.</a>
</div>

</body>
</html>
